<?php
Class book {
    private $DNS = "pgsql:host=localhost; port=5432; dbname=dbbis";
    private $user = "postgres";
    private $pass = "postgres";
    private $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,PDO::ATTR_DEFAULT_FETCH_MODE => PDO:: FETCH_ASSOC);
    protected $conn;

    public function db_connect(){
        try {
            $this->conn = new PDO($this->DNS, $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO:: FETCH_ASSOC);
       
            return $this->conn;
        }
        catch (PDOException $e) {
            echo "Err in connection: ". $e->getMessage();
        }finally{
            $this->conn = null;
        }
    }

    public function db_close() {
        $this->conn = null;
    }

    public function show_404() {
        http_response_code(404);
    }

    public function getBookDetails($authors_name) {
                  
        $connection = $this->db_connect();
        $stmt = "";
        $bookDet = null;
        $bookDetCnt = 0;
        $stmt = $connection->query("SELECT au.author_name,coalesce(bk.book_title ,'<none> (no books found)') as book_title FROM books bk RIGHT JOIN authors au ON au.author_id = bk.author_id where upper(au.author_name) ~ upper('$authors_name') order by au.author_name");

        $bookDet = $stmt->fetchAll();
        $bookDetCnt = $stmt->rowCount();
        if ($bookDetCnt > 0){
            return $bookDet;
        }else{
            return 0;
        }
    }

    public function addRecord($tablename,$id,$value){

        $connection = $this->db_connect();
        $stmt = "";
        $bookDet = null;
        $bookDetCnt = 0;
        $auth_id = $id;
        if($tablename=="AUTHORS"){
            $stmt = $connection->query("INSERT INTO AUTHORS (author_name) VALUES ('$value') returning author_id");

            $bookDet = $stmt->fetch();
            $bookDetCnt = $stmt->rowCount();

            if ($bookDetCnt != 0){
               return $bookDet["author_id"];
           }else{
               return 0;
           }
                        
        }else{
            $stmt = $connection->query("INSERT INTO BOOKS(author_id, book_title) VALUES ($auth_id,'$value')");

            $bookDet = $stmt->fetchAll();
            $bookDetCnt = $stmt->rowCount();

            if ($bookDetCnt > 0){
                return $bookDet;
            }else{
                return 0;
            }
        }
        
        
       // echo "ADD-Count:".$bookDetCnt . " tablename:". $tablename ." author_id:". $auth_id." value:".$value ."\n";

    }

    public function updateRecord($tablename,$book_id,$author_id,$value){

        $connection = $this->db_connect();
        $stmt = "";
        $bookDet = null;
        $bookDetCnt = 0;
        if($tablename=="AUTHORS"){
            $stmt = $connection->query("UPDATE authors SET author_name='$value' WHERE author_id = $author_id");
        }else{
            $stmt = $connection->query("UPDATE books SET author_id=$author_id, book_title='$value' WHERE book_id=$book_id");
        }
 

        $bookDet = $stmt->fetchAll();
        $bookDetCnt = $stmt->rowCount();

        //echo "UPDATE-Count:".$bookDetCnt . " tablename:". $tablename . " book_id:".$book_id ." author_id:". $author_id." value:".$value ."\n";


        if ($bookDetCnt > 0){
            return $bookDet;
        }else{
            return 0;
        }
    }
    
    public function checkIfExist($tablename,$value){
        $connection = $this->db_connect();
        $stmt = "";
        $bookDet = null;
        $bookDetCnt = 0;

        if($tablename=="AUTHORS"){
            $stmt = $connection->query("SELECT author_id as id FROM authors where trim(upper(author_name)) = trim(upper('$value'))");
        }elseif($tablename=="BOOKS"){                
            $stmt = $connection->query("SELECT book_id as id FROM books where trim(upper(book_title)) = trim(upper('$value'))");
        }

        $bookDet = $stmt->fetch();
        $bookDetCnt = $stmt->rowCount();
      
      // echo "rCount:".$bookDetCnt . " ". " tablename:". $tablename . "value:".$value ."\n";

       if ($bookDetCnt != 0){
            return $bookDet["id"];
       }else{
           return 0;
       }
    }

    public function load_xml_file(){


       $my_xmlfile = "./uploads/The Greatest Books of All Time.xml";
       //$my_xmlfile = 'http://suggestqueries.google.com/complete/search?client=toolbar&hl=ru&q=' . urlencode('Я');
      // $my_xmlfile = 'http://suggestqueries.google.com/complete/search?client=toolbar&hl=ar&q=' . urlencode('من هو');
       $xml =simplexml_load_string(utf8_encode(file_get_contents($my_xmlfile)));
 
       foreach ($xml as $record) {
           $author_name = trim($record->author);
           $book_title   = trim($record->name);
           
            $newAuthorId = 0;
            $tblAuthor = "AUTHORS";
            $author_id = $this->checkIfExist($tblAuthor,$author_name);
            if ($author_id > 0){
                $this->updateRecord($tblAuthor,0,$author_id,$author_name); 
                //echo "$tblAuthor" . " : " . $author_name . " : " . $author_id . " --> Existing author" . "\n";
            }else{
                $this->addRecord($tblAuthor,0,$author_name);
                //echo "$tblAuthor" . " : " . $author_name . " : " . $author_id . " --> Not Existing author" . "\n";
            }

            $tblBook = "BOOKS";
            $book_id = $this->checkIfExist($tblBook,$book_title);
            if ($book_id > 0){
                $this->updateRecord($tblBook,$book_id,$author_id,$book_title); 
                //echo "$tblBook" . " : " . $book_title . " : " .  $book_id . " --> Existing book" . "\n";
            }else{
                $this->addRecord($tblBook,$author_id ,$book_title);
                //echo "$tblBook" . " : " . $book_title . " : " . $book_id . " --> Not Existing book" . "\n";
            }
       }
    }
}

$book = new book();
