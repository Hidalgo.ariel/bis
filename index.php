<?php
   require_once('./bis.php');

   //Read and validate theXMLElement during initial of the page.
    $book->load_xml_file();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Information System</title>
    <link rel="stylesheet" href="./styles.css">

</head>
<body>
<h1>Book Information System</h1></br>
<hr></hr>
<div class="form-container">
<form action="" method="POST">
    <input type="text" name="search-input" id="search-input" class="search-input" placeholder="Search by Author">
    <button class="search-button" type="submit" name="submit" id="submit">Search </button>
  </form>
</div>
<div class="rec-container">
    <ul class="rec-list">
    </ul>
 </div>
</body>
<script>
    const recList = document.querySelector('.rec-list');
    const searchInput = document.querySelector('.search-input');
    const searchButton = document.querySelector('.search-button');
    searchButton.addEventListener('click', searchAuthor);

    function searchAuthor(e){
 
      e.preventDefault();

      <?php 

         $inputVal="";   
         $inputVal=isset($_POST['search-input'])?$_POST['search-input']:"";            
         $bookDets = $book->getBookDetails($inputVal);

     ?>
      <?php
          if($bookDets<>0) {
          foreach ($bookDets as $bookDet) {
      ?>
      
       searchDiv = document.createElement('div');
       searchDiv.classList.add('rec-info');
      
       //Create list element
       newRec = document.createElement('li');
       newRec.innerText = <?php echo "'".$bookDet['author_name']."'"; ?> ;
       newRec.classList.add('rec-au-item');
 

       //Create list element
       newRecBk = document.createElement('li');
       newRecBk.innerText = <?php echo "'".$bookDet['book_title']."'"; ?>;
       newRecBk.classList.add('rec-bk-item');


       //add li elements searchDiv
       searchDiv.appendChild(newRec);
       searchDiv.appendChild(newRecBk);

       //add All elements todo list 
       recList.appendChild(searchDiv);
        
      <?php      
         }
      }  
      ?>
    }    
 
</script>
</html>