-- --------------------------------------------------------
-- Database for BIS
-- --------------------------------------------------------

DROP DATABASE IF EXISTS dbbis;
CREATE DATABASE dbbis
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_Philippines.1252'
    LC_CTYPE = 'English_Philippines.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


-- --------------------------------------------------------
-- Table structure for table `authors
-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS public.authors
(
    author_id serial,
    author_name character varying(255) NOT NULL,
    CONSTRAINT authors_pkey PRIMARY KEY (author_id)
);

ALTER TABLE IF EXISTS public.authors OWNER to postgres;  

-- --------------------------------------------------------
-- Table structure for table `books
-- --------------------------------------------------------

CREATE TABLE public.books
(
    book_id serial,
    author_id bigint NOT NULL,
    book_title character varying(255) NOT NULL,
    PRIMARY KEY (book_id)
);

ALTER TABLE IF EXISTS public.books OWNER to postgres;